﻿namespace FormationSNCF.Vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.lIEUXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gESTIONLIEUXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aGENTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gESTIONAGENTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeAgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fORMATIONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionFormationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.activitéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lIEUXToolStripMenuItem,
            this.aGENTToolStripMenuItem,
            this.fORMATIONToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // lIEUXToolStripMenuItem
            // 
            this.lIEUXToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gESTIONLIEUXToolStripMenuItem});
            this.lIEUXToolStripMenuItem.Name = "lIEUXToolStripMenuItem";
            this.lIEUXToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.lIEUXToolStripMenuItem.Text = "LIEUX";
            // 
            // gESTIONLIEUXToolStripMenuItem
            // 
            this.gESTIONLIEUXToolStripMenuItem.Name = "gESTIONLIEUXToolStripMenuItem";
            this.gESTIONLIEUXToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.gESTIONLIEUXToolStripMenuItem.Text = "GESTION LIEUX";
            this.gESTIONLIEUXToolStripMenuItem.Click += new System.EventHandler(this.gestionDesLieuxToolStripMenuItem_Click_1);
            // 
            // aGENTToolStripMenuItem
            // 
            this.aGENTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gESTIONAGENTSToolStripMenuItem,
            this.listeAgentToolStripMenuItem});
            this.aGENTToolStripMenuItem.Name = "aGENTToolStripMenuItem";
            this.aGENTToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.aGENTToolStripMenuItem.Text = "AGENT";
            // 
            // gESTIONAGENTSToolStripMenuItem
            // 
            this.gESTIONAGENTSToolStripMenuItem.Name = "gESTIONAGENTSToolStripMenuItem";
            this.gESTIONAGENTSToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.gESTIONAGENTSToolStripMenuItem.Text = "GESTION AGENTS";
            this.gESTIONAGENTSToolStripMenuItem.Click += new System.EventHandler(this.AjouterUnAgentToolStripMenuItem_Click);
            // 
            // listeAgentToolStripMenuItem
            // 
            this.listeAgentToolStripMenuItem.Name = "listeAgentToolStripMenuItem";
            this.listeAgentToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.listeAgentToolStripMenuItem.Text = "Liste Agent";
            this.listeAgentToolStripMenuItem.Click += new System.EventHandler(this.listeAgentToolStripMenuItem_Click);
            // 
            // fORMATIONToolStripMenuItem
            // 
            this.fORMATIONToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionFormationToolStripMenuItem,
            this.gestionSessionToolStripMenuItem});
            this.fORMATIONToolStripMenuItem.Name = "fORMATIONToolStripMenuItem";
            this.fORMATIONToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.fORMATIONToolStripMenuItem.Text = "FORMATION";
            // 
            // actionFormationToolStripMenuItem
            // 
            this.actionFormationToolStripMenuItem.Name = "actionFormationToolStripMenuItem";
            this.actionFormationToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.actionFormationToolStripMenuItem.Text = "Action Formation";
            this.actionFormationToolStripMenuItem.Click += new System.EventHandler(this.fORMATIONToolStripMenuItem_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.activitéToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(284, 24);
            this.menuStrip2.TabIndex = 3;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // activitéToolStripMenuItem
            // 
            this.activitéToolStripMenuItem.Name = "activitéToolStripMenuItem";
            this.activitéToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.activitéToolStripMenuItem.Text = "Activité";
            this.activitéToolStripMenuItem.Click += new System.EventHandler(this.activitéToolStripMenuItem_Click);
            // 
            // gestionSessionToolStripMenuItem
            // 
            this.gestionSessionToolStripMenuItem.Name = "gestionSessionToolStripMenuItem";
            this.gestionSessionToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.gestionSessionToolStripMenuItem.Text = "Gestion Session ";
            this.gestionSessionToolStripMenuItem.Click += new System.EventHandler(this.gestionSessionToolStripMenuItem_Click);
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = global::FormationSNCF.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFenetrePrincipale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormationSNCF";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFentrePrincipale_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem lIEUXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gESTIONLIEUXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aGENTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gESTIONAGENTSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fORMATIONToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionFormationToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem activitéToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeAgentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionSessionToolStripMenuItem;
    }
}