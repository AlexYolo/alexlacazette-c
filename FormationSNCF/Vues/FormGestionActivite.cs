﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Ressources;
using FormationSNCF.Modele;

namespace FormationSNCF.Vues
{
    public partial class FormGestionActivite : Form
    {
        public FormGestionActivite()
        {
            InitializeComponent();
        }

        private void FormGestionActivite_Load(object sender, EventArgs e)
        {
            listBoxListeActivites.DataSource = Donnees.CollectionActivite;
        }



        private void buttonAjoutActivite_Click(object sender, EventArgs e)
        {
            if (textBoxNomActivite.Text.Length < 4)
            {
                MessageBox.Show("L'activité dois être composé d'au moins 3 caractère");
                textBoxNomActivite.Text = "";
                textBoxNomActivite.Focus();

            }

            else
            {

                listBoxListeActivites.DataSource = null;
                bool trouver = false;
                string nomActi = textBoxNomActivite.Text;
                foreach (Activite activiteCourant in Donnees.CollectionActivite)
                {
                    if (activiteCourant.LibelleActivite == nomActi)
                    {
                        MessageBox.Show("Déja existant");
                        trouver = true;
                    }
                }

                if (trouver == false)
                {
                    Activite uneActivte = new Activite(nomActi);
                    Donnees.CollectionActivite.Add(uneActivte);
                }
            }
            listBoxListeActivites.DataSource = Donnees.CollectionActivite;


        }
    }
}
