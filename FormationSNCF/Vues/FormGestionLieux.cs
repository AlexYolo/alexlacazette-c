﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;


namespace FormationSNCF.Vues
{
    public partial class FormGestionLieux : Form
    {
        public FormGestionLieux()
        {
            InitializeComponent();
        }
        private void FormGestionLieu_Load(object sender, EventArgs e)
        {

            DataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
        }

        private void ButtonAjoutLieu_Click(object sender, EventArgs e)
        {
            if (textBoxLibelle.Text.Length < 3)
            {
                MessageBox.Show("Le lieu doit être composé d'au moins 3 caractère");
                textBoxLibelle.Text= "";
                textBoxLibelle.Focus();
            }
            else if (textBoxTelephone.Text.Length ==0)
            {
                MessageBox.Show("Le numéro de téléphone est obligatoire");
                textBoxTelephone.Focus();
            }
            else if (textBoxCodepostal.Text.Length == 0)
            {

                MessageBox.Show("le code postal est obligatoire0");
                textBoxCodepostal.Focus();

            }
            else
            {
                DataGridViewListeLieu.DataSource = null;
                int numeroMax = 0;
                foreach (Lieu LieuCourant in Donnees.CollectionLieu)
                {
                    if (LieuCourant.Numero > numeroMax)
                        numeroMax = LieuCourant.Numero;

                }

                Lieu unlieu = new Lieu(numeroMax + 1, textBoxLibelle.Text, textBoxCodepostal.Text, textBoxTelephone.Text);
                Donnees.CollectionLieu.Add(unlieu);
                DataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
            }

        }
        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (textBoxCodepostal.Text.Length!= 0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodepostal.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffres");
                    textBoxCodepostal.Text = "";
                    textBoxCodepostal.Focus();
                }
            }
                

        }
        private void TextBoxTelephone_Leave(object sender, EventArgs e)
        {
            if (textBoxTelephone.Text !="")
            {
                if(!Formulaire.VerificationTelephone (textBoxTelephone.Text))
                {
                    MessageBox.Show("Saisir un numéro de téléphone a 10 chiffres");
                    textBoxTelephone.Text = "";
                    textBoxTelephone.Focus();
                }

            }


        }

        
 
       
    }
}
