﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;


namespace FormationSNCF.Vues
{
    public partial class FormFenetrePrincipale : Form
    {
        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }


        private Form _mdiChild;
        private Form Mdichild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }

        }



        private void FormFenetrePrincaleForm_Closing(object sender, FormClosedEventArgs e)
        {
            Donnees.SauvegardeDonnees();

        }
        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormGestionLieux();
        }

        private void gestionDesLieuxToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Mdichild = new FormGestionLieux();
        }
        private void AjouterUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Mdichild = new FormAjouterAgent();
        }


        private void fORMATIONToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormGestionActionFormation();
        }

        private void activitéToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormGestionActivite();
        }

       
        private void listeAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormListeAgent();
        }

        private void FormFentrePrincipale_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }

        private void gestionSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormGestionSessionDeFormation();
        }


       
      

    }
}
