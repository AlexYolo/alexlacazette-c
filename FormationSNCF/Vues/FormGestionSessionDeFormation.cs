﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Vues;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionSessionDeFormation : Form
    {
        public FormGestionSessionDeFormation()
        {
            InitializeComponent();
        }

        private void ButtonAjouterSession_Click(object sender, EventArgs e)
        {
            //if (!Numerique.IsDecimal(textBoxCoutHebergementSession.Text))
            //{
            //    MessageBox.Show("Saisir une valeur décimal");
            //}
            //else if (textBoxDateSession.Text == "")
            //{
            //    MessageBox.Show("Saisir une date");
            //}
            //else if (!Formulaire.VerificationFormatDate(textBoxDateSession.Text))
            //{
            //    MessageBox.Show("Format date incorrect \n Saisir une date au format suivant : jj/mm/aaaa ");
            //}
            //else if (!Numerique.IsNumerique(textBoxNbParticipantSession.Text))
            //{
            //    MessageBox.Show("Saisir une valeur numérique entière ");
            //}

            //else
            //{
            //    if (ActionFormationSelectionnee != null)
            //    {
            //        int nextNum = 0;
            //        if (ActionFormationSelectionnee.SessionsFormation.Count != 0)
            //        {
            //            foreach (SessionFormation sessionFormationCourante in ActionFormationSelectionnee.SessionsFormation)
            //            {
            //                if (nextNum < sessionFormationCourante.Numero)
            //                    nextNum = sessionFormationCourante.Numero;
            //            }
            //            nextNum++;
            //        }

            //        byte nbPart;
            //        byte.TryParse(textBoxNbParticipantSession.Text, out nbPart);
            //        DateTime date;
            //        DateTime.TryParse(textBoxDateSession.Text, out date);
            //        decimal coutHebergement;
            //        decimal.TryParse(textBoxCoutHebergementSession.Text, out coutHebergement);

            //        ActionFormationSelectionnee.AjouterSessionDeFormation(nextNum, nbPart, date, coutHebergement);
            //        ActionFormationSelectionnee.ObtenirSessionFormation(nextNum).LieuDeFormation = comboBoxListeLieu.SelectedItem as Lieu;
            //        ViderChampsTexte();

            //        dataGridViewSessionsDeFormation.DataSource = ActionFormationSelectionnee.SessionsFormation;
            //    }

            //}

        }
        
        private void FormGestionSessionDeFormation_Load(object sender, EventArgs e)
        {
            comboBoxListeActivite.DataSource = Donnees.CollectionActivite;
            comboBoxListeActivite.DisplayMember = "LibelleActivite";

            comboBoxListeLieu.DataSource = Donnees.CollectionLieu;
            comboBoxListeLieu.DisplayMember = "LibelleLieu";
            comboBoxListeLieu.ValueMember = "Numero";
        }

        private void GestionActionFormation_load (object sender, EventArgs e)
        {
            comboBoxListeActivite.DataSource = Donnees.CollectionActivite;
            comboBoxListeActivite.DisplayMember = "LibelleActivité";
        }

    }
}
